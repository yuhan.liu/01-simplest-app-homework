package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TableController {
    @GetMapping("/api/tables/plus")
    public String getPlusTable() {
        String plusTable = "";
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                plusTable += (j + "+" + i + "=" + (i + j) + " ");
            }plusTable +=("</br>");
        }return plusTable;
    }
    @GetMapping("/api/tables/multiply")
    public String getMultiplyTable() {
        String MultiplyTable = "";
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                MultiplyTable += (j + "*" + i + "=" + i * j + " ");
            }MultiplyTable +=("</br>");
        }return MultiplyTable;
    }
}
