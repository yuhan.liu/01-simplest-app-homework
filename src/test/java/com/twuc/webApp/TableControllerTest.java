package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
class TableControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_plus_table() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/api/tables/plus")).andReturn().getResponse();
        assertThat(response.getContentAsString()).isEqualTo(
                "1+1=2 </br>" +
                        "1+2=3 2+2=4 </br>" +
                        "1+3=4 2+3=5 3+3=6 </br>" +
                        "1+4=5 2+4=6 3+4=7 4+4=8 </br>" +
                        "1+5=6 2+5=7 3+5=8 4+5=9 5+5=10 </br>" +
                        "1+6=7 2+6=8 3+6=9 4+6=10 5+6=11 6+6=12 </br>" +
                        "1+7=8 2+7=9 3+7=10 4+7=11 5+7=12 6+7=13 7+7=14 </br>" +
                        "1+8=9 2+8=10 3+8=11 4+8=12 5+8=13 6+8=14 7+8=15 8+8=16 </br>" +
                        "1+9=10 2+9=11 3+9=12 4+9=13 5+9=14 6+9=15 7+9=16 8+9=17 9+9=18 </br>");
    }

    @Test
    void should_get_multiply_table() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/api/tables/multiply")).andReturn().getResponse();
        assertThat(response.getContentAsString()).isEqualTo(
                "1*1=1 </br>" +
                        "1*2=2 2*2=4 </br>" +
                        "1*3=3 2*3=6 3*3=9 </br>" +
                        "1*4=4 2*4=8 3*4=12 4*4=16 </br>" +
                        "1*5=5 2*5=10 3*5=15 4*5=20 5*5=25 </br>" +
                        "1*6=6 2*6=12 3*6=18 4*6=24 5*6=30 6*6=36 </br>" +
                        "1*7=7 2*7=14 3*7=21 4*7=28 5*7=35 6*7=42 7*7=49 </br>" +
                        "1*8=8 2*8=16 3*8=24 4*8=32 5*8=40 6*8=48 7*8=56 8*8=64 </br>" +
                        "1*9=9 2*9=18 3*9=27 4*9=36 5*9=45 6*9=54 7*9=63 8*9=72 9*9=81 </br>");
    }
}
